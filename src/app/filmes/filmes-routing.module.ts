import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilmesListaComponent } from './filmes-lista/filmes-lista.component';


const routes: Routes = [
  {
    path: '',
    component: FilmesListaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilmesRoutingModule { }
