import { Component, OnInit, TemplateRef } from '@angular/core';
import { ServicosService } from 'src/app/compartilhado/servicos/servicos.service';
import { catchError } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MultiDataSet, Label } from 'ng2-charts';
import { ChartType } from 'chart.js';

@Component({
  selector: 'app-filmes-lista',
  templateUrl: './filmes-lista.component.html',
  styleUrls: ['./filmes-lista.component.scss']
})
export class FilmesListaComponent implements OnInit {

  bsModalRef: BsModalRef;

  conteudo: string;
  linguagem: string;

  url_img: string = 'http://image.tmdb.org/t/p/w185';

  filmes: Subscription;

  imagem: string;
  title: string;

  filmesLista: any[];
  modalRef: BsModalRef;

  public doughnutChartLabels: Label[];
   public doughnutChartData: MultiDataSet;
   public doughnutChartType: ChartType = 'doughnut';

  constructor(
    private servicos: ServicosService,
    private modalService: BsModalService
  ) { 
    this.doughnutChartLabels;
    this.doughnutChartData;
  }

  ngOnInit(): void {
    this.listaFilmes()
  }

  listaFilmes() {
    this.filmes = this.servicos.getFilmes()
      .pipe(
        catchError(erros => of(console.log(erros)))        
      )
      .subscribe(res => {
        this.filmesLista = res['results'];
        console.log(res['results'])})
  }

  ngOnDestroy(): void {
    this.filmes.unsubscribe();
  }

  detalheFilme(template: TemplateRef<any>, id: number) {
    
    this.servicos.getFilmeDetalhe(id)
      .pipe(
        catchError(err => of(console.log(err)))
      ).subscribe(res => {
        
        let genero = res['genres'].map(res => res['id']);
        let generoName = res['genres'].map(res => res['name']);
      
        this.doughnutChartLabels = generoName;
        this.doughnutChartData = genero
        this.title = res['original_title'];
        this.imagem = res['poster_path'];
        this.conteudo = res['overview'];
        this.linguagem = res['original_language'];
        this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
      })
  }


}
