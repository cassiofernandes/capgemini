import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilmesRoutingModule } from './filmes-routing.module';
import { FilmesListaComponent } from './filmes-lista/filmes-lista.component';
import { LimiterCaracterPipe } from '../pipes/limiter-caracter.pipe';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [FilmesListaComponent, LimiterCaracterPipe],
  imports: [
    CommonModule,
    FilmesRoutingModule,
    ChartsModule
  ]
})
export class FilmesModule { }
