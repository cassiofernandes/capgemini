import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormDetalheComponent } from './form-detalhe/form-detalhe.component';


const routes: Routes = [
  {
    path: '',
    component: FormDetalheComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingModule { }
