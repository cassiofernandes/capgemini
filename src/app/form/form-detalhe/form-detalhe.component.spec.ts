import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDetalheComponent } from './form-detalhe.component';

describe('FormDetalheComponent', () => {
  let component: FormDetalheComponent;
  let fixture: ComponentFixture<FormDetalheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDetalheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
