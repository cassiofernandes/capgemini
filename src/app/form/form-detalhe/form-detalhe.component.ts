import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-form-detalhe',
  templateUrl: './form-detalhe.component.html',
  styleUrls: ['./form-detalhe.component.scss']
})
export class FormDetalheComponent implements OnInit {

  profileForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    senha: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]]
  });

  get email() {
    return this.profileForm.get('email');
  }

  get senha() {
    return this.profileForm.get('senha');
  }

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  
  onSubmit() {
    alert(JSON.stringify(this.profileForm.value))
  }
}
