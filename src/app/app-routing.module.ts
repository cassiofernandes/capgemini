import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { 
    path: 'filmes', 
    loadChildren: () => import('./filmes/filmes.module').then( m => m.FilmesModule )
  },
  {
    path: 'form',
    loadChildren: () => import('./form/form.module').then( m => m.FormModule )
  },
  {
    path: '',
    redirectTo: '/filmes',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
