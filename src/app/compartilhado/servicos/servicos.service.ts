import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicosService {

  private readonly API_KEY = 'aa7a7d31eb16c5f13021ce3c2df7e19a';

  private readonly API_FILMES = 'https://api.themoviedb.org/3/movie/popular?api_key=';

  private readonly API_FILME_DETALHE = 'https://api.themoviedb.org/3/movie/';

  constructor(
    private http: HttpClient
  ) { }

  getFilmes() {
    return this.http.get(this.API_FILMES + this.API_KEY + '&language=pt-BR&page=1')
  }
  getFilmeDetalhe(id: number) {
    return this.http.get(this.API_FILME_DETALHE + id + '?api_key=' + this.API_KEY + '&language=pt-BR')
  }

}
